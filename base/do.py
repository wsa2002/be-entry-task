from datetime import datetime

from pydantic import BaseModel


class Post(BaseModel):
    id: int
    name: str
    content: str
    time: datetime


class Comment(BaseModel):
    id: int
    post_id: int
    name: str
    message: str
    time: datetime
