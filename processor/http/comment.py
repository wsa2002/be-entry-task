from typing import Sequence

from sqlalchemy import pool
from fastapi import Depends
from pydantic import BaseModel

from base import do
from middleware import APIRouter, JSONResponse, enveloped
from persistence.database import get_pool
import persistence.database as database
from util.context import context

router = APIRouter(
    tags=['Comment'],
    default_response_class=JSONResponse,
)


@router.get('/post/{post_id}/comment')
@enveloped
def browse_comment(post_id: int, db: pool.Pool = Depends(get_pool)) -> Sequence[do.Comment]:
    comments = database.comment.browse(db=db, post_id=post_id)
    return [do.Comment(id=comment.id, name=comment.name, message=comment.message,
                       time=comment.time, post_id=comment.post_id)
            for comment in comments]


class AddCommentInput(BaseModel):
    name: str
    message: str


class AddCommentOutput(BaseModel):
    id: int


@router.post('/post/{post_id}/comment')
@enveloped
def add_comment(post_id: int, data: AddCommentInput, db: pool.Pool = Depends(get_pool)) -> AddCommentOutput:
    comment_id = database.comment.add(db=db, post_id=post_id, name=data.name,
                                      message=data.message, time=context.request_time)
    return AddCommentOutput(id=comment_id)
