from sqlalchemy import Column, Integer, VARCHAR, TEXT, TIMESTAMP, ForeignKey
from datetime import datetime

from persistence.database import Base


class Comment(Base):
    __tablename__ = 'comment'

    id = Column(Integer, primary_key=True, autoincrement=True, index=True)
    name = Column(VARCHAR, nullable=False)
    message = Column(TEXT, nullable=False)
    time = Column(TIMESTAMP, nullable=False)
    post_id = Column(Integer, ForeignKey("post.id"), index=True)

    def __init__(self, name: str, message: str, post_id: int, time: datetime, *args, **kwargs):
        self.name = name
        self.message = message
        self.time = time
        self.post_id = post_id
        super().__init__(*args, **kwargs)
