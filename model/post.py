from sqlalchemy import Column, Integer, VARCHAR, TEXT, TIMESTAMP
from datetime import datetime

from persistence.database import Base


class Post(Base):
    __tablename__ = 'post'

    id = Column(Integer, primary_key=True, autoincrement=True, index=True)
    name = Column(VARCHAR, nullable=False)
    content = Column(TEXT, nullable=False)
    time = Column(TIMESTAMP, nullable=False)

    def __init__(self, name: str, content: str, time: datetime, *args, **kwargs):
        self.name = name
        self.content = content
        self.time = time
        super().__init__(*args, **kwargs)
