from datetime import datetime
from typing import Sequence

from sqlalchemy import pool
from sqlalchemy.orm import sessionmaker

import model


def browse(db: pool.Pool, post_id: int) -> Sequence[model.Comment]:
    with sessionmaker(db)() as session:
        return session.query(model.Comment).filter_by(post_id=post_id).order_by(model.Comment.id).all()


def add(db: pool.Pool, post_id: int, name: str, message: str, time: datetime) -> int:
    with sessionmaker(db)() as session:
        comment = model.Comment(post_id=post_id, name=name, message=message, time=time)
        session.add(comment)
        session.commit()
        session.refresh(comment)
        return comment.id
