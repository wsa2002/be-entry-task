from typing import Sequence

from sqlalchemy import pool
from fastapi import Depends
from pydantic import BaseModel

from base import do
from middleware import APIRouter, JSONResponse, enveloped
from persistence.database import get_pool
import persistence.database as database
from util.context import context

router = APIRouter(
    tags=['Post'],
    default_response_class=JSONResponse,
)


@router.get('/post')
@enveloped
def browse_post(db: pool.Pool = Depends(get_pool)) -> Sequence[do.Post]:
    posts = database.post.browse(db)
    return [do.Post(id=post.id, content=post.content, name=post.name,
                    time=post.time)
            for post in posts]


class AddPostInput(BaseModel):
    name: str
    content: str


class AddPostOutput(BaseModel):
    id: int


@router.post('/post')
@enveloped
def add_post(data: AddPostInput, db: pool.Pool = Depends(get_pool)) -> AddPostOutput:
    id_ = database.post.add(db=db, name=data.name, content=data.content, time=context.request_time)
    return AddPostOutput(id=id_)


@router.get('/post/{post_id}')
@enveloped
def read_post(post_id: int, db: pool.Pool = Depends(get_pool)) -> do.Post:
    post = database.post.read(db=db, post_id=post_id)
    return do.Post(id=post.id, name=post.name, content=post.content, time=post.time)


class EditPostInput(BaseModel):
    content: str | None


@router.patch('/post/{post_id}')
@enveloped
def edit_post(post_id: int, data: EditPostInput, db: pool.Pool = Depends(get_pool)) -> None:
    return database.post.edit(db=db, post_id=post_id, content=data.content)


@router.delete('/post/{post_id}')
@enveloped
def delete_post(post_id: int, db: pool.Pool = Depends(get_pool)) -> None:
    return database.post.delete(db=db, post_id=post_id)
