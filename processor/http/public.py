from fastapi.responses import HTMLResponse

from middleware import APIRouter, JSONResponse
from config import app_config

router = APIRouter(
    tags=['Public'],
    default_response_class=JSONResponse,
)


@router.get('/', response_class=HTMLResponse)
def default_page():
    doc_paths = []
    if _url := app_config.docs_url:
        doc_paths.append(f'<a href="{_url}">{_url}</a>')
    if _url := app_config.redoc_url:
        doc_paths.append(f'<a href="{_url}">{_url}</a>')
    return fr"""
    {' or '.join(doc_paths)}
    """

