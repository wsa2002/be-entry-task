import sqlalchemy.pool
from sqlalchemy.ext.declarative import declarative_base

from base import mcs
from config import DBConfig


class PoolHandler(metaclass=mcs.Singleton):
    def __init__(self):
        self._pool: sqlalchemy.pool.Pool = None

    def initialize(self, db_config: DBConfig):
        if self._pool is None:
            self._pool = sqlalchemy.create_engine(
                url=db_config.db_url,
            )

    def close(self):
        if self._pool is not None:
            self._pool.dispose()

    @property
    def pool(self):
        return self._pool


pool_handler = PoolHandler()
Base = declarative_base()


def get_pool():
    return pool_handler.pool


from . import (
    post,
    comment,
)
