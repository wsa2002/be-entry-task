import json
import typing

import fastapi.responses
from fastapi.encoders import jsonable_encoder


class JSONEncoder(json.JSONEncoder):
    def default(self, obj):
        return jsonable_encoder(obj)


class JSONResponse(fastapi.responses.JSONResponse):
    def __init__(self, *args, success=True, error: Exception = None, **kwargs):
        self._success = success
        self._error = error
        super().__init__(*args, **kwargs)

    def render(self, content: typing.Any) -> bytes:
        return json.dumps(
            content,
            cls=JSONEncoder,
            ensure_ascii=False,
            allow_nan=False,
            indent=None,
            separators=(",", ":"),
        ).encode("utf-8")
