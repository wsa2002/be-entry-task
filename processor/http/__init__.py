import fastapi


def register_routers(app: fastapi.FastAPI):
    from . import (
        post,
        comment,
        public,
    )

    app.include_router(public.router)

    app.include_router(post.router)
    app.include_router(comment.router)

