from . import NormalException, _CauseMixin


class NotFound(NormalException):
    """
    Object not found, e.g. asked data not found in database
    """