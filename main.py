
with open('logging.yaml', 'r') as config:
    import yaml
    log_config = yaml.safe_load(config.read())

    import logging.config
    logging.config.dictConfig(log_config)

import log

from config import app_config
from middleware.api import FastAPI

app = FastAPI(
    title=app_config.title,
    docs_url=app_config.docs_url,
    redoc_url=app_config.redoc_url,
)


@app.on_event('startup')
def app_startup():
    from persistence.database import pool_handler, Base
    from config import db_config

    log.info('Database initializing')
    pool_handler.initialize(db_config=db_config)
    Base.metadata.create_all(pool_handler.pool)
    log.info('Database initialized')


@app.on_event('shutdown')
def app_shutdown():
    from persistence.database import pool_handler
    pool_handler.close()


# import middleware.auth
# app.middleware('http')(middleware.auth.middleware)

import middleware.tracker
app.middleware('http')(middleware.tracker.middleware)

import starlette_context.middleware
app.add_middleware(starlette_context.middleware.RawContextMiddleware)

import middleware.envelope
middleware.envelope.hook_exception_envelope_handler(app)


import processor.http
processor.http.register_routers(app)
