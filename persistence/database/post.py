from typing import Sequence
from datetime import datetime

from sqlalchemy import pool
from sqlalchemy.orm import sessionmaker

import model
import exceptions as exc


def browse(db: pool.Pool) -> Sequence[model.Post]:
    with sessionmaker(db)() as session:
        return session.query(model.Post).order_by(model.Post.id).all()


def add(db: pool.Pool, name: str, content: str, time: datetime) -> int:
    with sessionmaker(db)() as session:
        post = model.Post(name=name, content=content, time=time)
        session.add(post)
        session.commit()
        session.refresh(post)
        return post.id


def read(db: pool.Pool, post_id: int) -> model.Post:
    with sessionmaker(db)() as session:
        if not (post := session.query(model.Post).filter_by(id=post_id).first()):
            raise exc.persistence.NotFound
        return post


def edit(db: pool.Pool, post_id: int, content: str = None, time: datetime = datetime.now()) -> None:
    to_update = {}

    if content is not None:
        to_update['content'] = content

    if not to_update:
        return
    to_update['time'] = time

    with sessionmaker(db)() as session:
        session.query(model.Post).filter_by(id=post_id).update(to_update)
        session.commit()


def delete(db: pool.Pool, post_id: int) -> None:
    with sessionmaker(db)() as session:
        session.query(model.Post).filter_by(id=post_id).delete()
        session.commit()
