import os

from dotenv import dotenv_values

env_values = {
    **dotenv_values(".env"),
    **os.environ,
}


class DBConfig:
    db_url = env_values.get('PG_URL')


class AppConfig:
    title = env_values.get('APP_TITLE')
    docs_url = env_values.get('APP_DOCS_URL')
    redoc_url = env_values.get('APP_REDOC_URL')


class LoggerConfig:
    event_logger_name = env_values.get('EVENT_LOGGER_NAME')
    timing_logger_name = env_values.get('TIMING_LOGGER_NAME')


class JWTConfig:
    secret = env_values.get('JWT_SECRET')
    encode_algorithm = env_values.get('JWT_ENCODE_ALGORITHM')
    expire_days = env_values.get('JWT_EXPIRE_DAYS')


app_config = AppConfig()
db_config = DBConfig()
logger_config = LoggerConfig()
jwt_config = JWTConfig()
