from functools import wraps
import logging

import traceback

from config import logger_config
from util.context import context


class _Logger:
    """
    Logger is initialized in config_logger
    """
    event_logger = logging.getLogger(logger_config.event_logger_name)
    timing_logger = logging.getLogger(logger_config.timing_logger_name)


# Event logging


def info(msg):
    _Logger.event_logger.info(f"request {context.get_request_uuid()}\t{msg}")


def debug(msg):
    _Logger.event_logger.debug(f"request {context.get_request_uuid()}\t{msg}")


def error(msg):
    _Logger.event_logger.error(f"request {context.get_request_uuid()}\t{msg}")


def exception(exc: Exception, msg='', info_level=False):
    if info_level:
        _Logger.event_logger.info(f"{format_exc(exc)}\n{traceback.format_exc()}")
    else:
        _Logger.event_logger.error(f"request {context.get_request_uuid()}\t{msg}\t{exc.__repr__()}")
        _Logger.event_logger.exception(exc)


def format_exc(e: Exception):
    return f"{type(e).__name__}: {e}"
